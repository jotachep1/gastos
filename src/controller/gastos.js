//const gastos = require('../sample.json');
//const servicesGastos = require('../service/gastos');
const Gasto = require('../models/gastos');
exports.getGastos = async(req, res) => {
    const { id = null } = req.query
    const busqueda = id ? { _id: id } : {}
    const listado = await Gasto.find(busqueda)
        .then((result) => {
            console.log("ok")
            console.log(result)
        }).catch((err) => {
            console.log("error")
            console.log(err)
        });

    //const listado = servicesGastos.obtenerGastos(busqueda)
    res.json(listado)
};

exports.postGastos = async(req, res) => {
    const { fecha, Monto, Lugar, Descripcion } = req.body
    const gasto = {}
    gasto.fecha = fecha
    gasto.Monto = Monto
    gasto.Lugar = Lugar
    gasto.Descripcion = Descripcion
    const newGasto = servicesGastos.obtenerGastos(gasto)
    return newGasto;
};

/*exports.postApiV1UsuariosRecuperar_clave = async(req, res) => {};*/