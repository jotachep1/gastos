const { Schema } = require('mongoose')

exports.gastosSchema = new Schema({
    fecha: {
        type: String,
        required: true
    },
    Monto: {
        type: Number,
        required: true
    },
    Lugar: {
        type: String
    },
    Descripcion: {
        type: Number
    },
    required: false,
    request: {
        type: JSON,
        required: true
    },
    response: {
        type: JSON,
        required: true
    }
})