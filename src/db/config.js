const mongoose = require('mongoose');

exports.conectar = async() => {
    const cnn = process.env.MONGODB_CNN ? process.env.MONGODB_CNN : ''
    try {
        await mongoose.connect(cnn, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        console.log('Base de datos de datos online');

    } catch (error) {
        throw new Error('Error en base de datos')
    }
}