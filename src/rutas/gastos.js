const { Router } = require('express');
const { getGastos, postGastos /*, putGastos, deleteGastos*/ } = require('../controller/gastos')
const router = Router();
// Rutas
router.get('/', getGastos)
router.post('/', postGastos)

module.exports = router;