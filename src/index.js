const dotenv = require('dotenv');
dotenv.config()
const express = require('express');
const app = express();
const morgan = require('morgan');
const dbConnection = require('./db/config')
    //configuraciones
app.set('port', process.env.PORT || 3001)

dbConnection.conectar();

app.set('json spaces', 2)
app.set('views', '/vistas')
app.set('view engine', 'ejs')


// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//Rutas
app.use(require('./rutas/index'))
app.use('/api/gastos', require('./rutas/gastos'))

// iniciando el servidor
app.listen(app.get('port'), () => {
    console.log("server arriba en puerto ", app.get('port'))
})